from flask import Flask
from flask_restful import Resource, Api, reqparse

app=Flask(__name__)
api= Api(app)

variabel = [
    {"client_id": 1, "client_key": "CLIENT01", "client_secret": "SECRET01", "status": True},
    {"client_id": 2, "client_key": "CLIENT02", "client_secret": "SECRET02", "status": False},
    {"client_id": 3, "client_key": "CLIENT03", "client_secret": "SECRET03", "status": False},
    {"client_id": 4, "client_key": "CLIENT04", "client_secret": "SECRET04", "status": False}
]

class Clients(Resource):
    def get(self):
        return variabel

api.add_resource(Clients, "/clients")

class Client(Resource):

    def get(self, get_by_client_id):
        for item in variabel:
            if get_by_client_id == item["client_id"]:
                return item
        else:
            return "DATA NOT FOUND", 404

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("client_id", type= int, help= "Client id must be integer", location= "json", required= True)
        parser.add_argument("client_key", type= str, help= "Client key must be string", location= "json", required= True)
        parser.add_argument("client_secret", type= str, help= "Client secret must be string", location= "json", required= True)
        parser.add_argument("status", type= bool, help= "Status must be boolean", location= "json", required= True)
        
        args = parser.parse_args()
        
        variabel.append(args)

        return variabel, 201

    def put(self, get_by_client_id):
        parser = reqparse.RequestParser()

        parser.add_argument("client_key", type= str, help= "Client key must be string", location= "json", required= True)
        parser.add_argument("client_secret", type= str, help= "Client secret must be string", location= "json", required= True)
        parser.add_argument("status", type= bool, help= "Status must be boolean", location= "json", required= True)

        args = parser.parse_args()

        found = False

        for item in variabel:
            if item["client_id"] == get_by_client_id:
                found = True
                item["client_key"]= args["client_key"]
                item["client_secret"]= args["client_secret"]
                item["status"]= args["status"]

        if not found:
            return "DATA NOT EXIST", 404

        return args, 200

    def delete(self, get_by_client_id):
        idx = 0
        idx_data = 0
        found = False
        for item in variabel:
            if item["client_id"] == get_by_client_id:
                found = True
                idx_data = idx
            idx += 1

        if not found:
            return "DATA NOT FOUND", 404
        else:
            variabel.pop(idx_data)
            return "DATA DELETED", 200

api.add_resource(Client, "/client","/client/<int:get_by_client_id>")

if __name__ == '__main__':
    app.run(debug=True, host="127.0.0.1", port=5000)